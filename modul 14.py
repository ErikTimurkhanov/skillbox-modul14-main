# задача 2
print("Введите первую точку")

x1 = float(input('X: '))
y1 = float(input('Y: '))
print("\nВведите вторую точку")
x2 = float(input('X: '))
y2 = float(input('Y: '))

x_diff = x1 - x2
y_diff = y1 - y2
if x_diff == 0:
    k = 1
else:
    k = y_diff / x_diff
b = y2 - k * x2

print("Уравнение прямой, проходящей через эти точки:")
print("y = ", k, " * x + ", b)

# задача 3

def sum_N(N):
    count = 0
    while N > 0:
        n = N % 10
        count += n
        N //= 10
    print('Сумма чисел: ', count)
    return count

def count_N(N):
    count = 0
    while N > 0:
        n = N // 10
        count += 1
        N //= 10
    print('Количество цифр в числе:  ', count)
    return count
N = int(input('Введите число N '))
summ = sum_N(N)
count = count_N(N)
res = summ - count
print('Разность суммы и количества цифр: ', res)

# задача 4
def num_mun(g):
    g = int(g)
    if g != 0:
        count = ''
        while g > 0:
            last_num = str(g % 10)
            count += last_num
            g = g // 10
    #print('Число наоборот:', count)
    return count


def number(p):
    text_x = ''
    text_y = ''

    for i in str(p):
        if i == '.':
            break
        text_x += i
    x = num_mun(text_x)

    for i in str(p):
        if i == '.':
            text_y = ''
        text_y += i
    text_y = text_y[1:]
    y = num_mun(text_y)

    return str(x + '.' + y)


N = input('Введите число N ')
K = input('Введите число K ')

n = number(N)
print('Число наоборот:', n)

k = number(K)
print('Число наоборот:', k)

res = float(k) + float(n)
print('Сумма чисел:', res)

# задача 5
def divider():
    for i in range(2, n+1, 1):
        if n % i == 0:
            print('Наименьший делитель, отличный от единицы:',i)
            break

n = int(input('Введите натуральное число: '))
divider()

# задача 6
def x_y_point():
    c = (x1 ** 2 + y1 ** 2) ** 0.5
    return c

x1 = float(input('введите значение Х: '))
y1 = float(input('введите значение Y: '))
r = float(input('введите радиус: '))
c = x_y_point()

if c <= r:
    print('Монетка где-то рядом')

else:
    print('Монетки в области нет')

# задача 7
def year(A, B):
    for w in range(A, B + 1):
        # print(w)
        num1 = w % 10
        num2 = w // 10 % 10
        num3 = w // 100 % 10
        num4 = w // 1000

        if num1 == num2 == num3 or num1 == num2 == num4 or num4 == num2 == num3 or num1 == num4 == num3:
            print(w)

A = int(input('Введите первый год: '))
B = int(input('Введите второй год: '))
print(f'Годы от {A} до {B} с тремя одинаковыми цифрами:')
year(A, B)